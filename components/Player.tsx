import {
  SwitchHorizontalIcon,
  VolumeUpIcon as VolumeDownIcon,
} from "@heroicons/react/outline";
import {
  FastForwardIcon,
  PauseIcon,
  PlayIcon,
  ReplyIcon,
  RewindIcon,
  VolumeUpIcon,
} from "@heroicons/react/solid";
import { debounce } from "lodash";
import { useSession } from "next-auth/react";
import { useCallback, useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import { currentTrackIdState, isPlayingState } from "../atoms/songAtom";
import useSongInfo from "../hooks/useSongInfo";
import useSpotify from "../hooks/useSpotify";

function Player() {
  const spotifyApi = useSpotify();
  const { data: session, status } = useSession();
  const [currentTrackId, setCurrentIdTrack] =
    useRecoilState(currentTrackIdState);
  const [isPlaying, setIsPlaying] = useRecoilState(isPlayingState);
  const [volume, setVolume] = useState(50);
  const [trackPosition, setTrackPosition] = useState<number>();
  const [shuffle, setShuffle] = useState(false);
  const [repeat, setRepeat] = useState("off");
  const [songLength, setSongLength] = useState(0);
  const [repeatColor, setRepeatColor] = useState("text-white");
  const songInfo = useSongInfo();

  const fetchCurrentSong = () => {
    if (!songInfo) {
      spotifyApi.getMyCurrentPlayingTrack().then((data) => {
        console.log(data);
        console.log("Now Playing: ", data.body?.item);

        setCurrentIdTrack(data.body?.item?.id);
        setSongLength(Math.floor(data.body?.item.duration_ms));

        spotifyApi.getMyCurrentPlaybackState().then((data) => {
          setIsPlaying(data.body?.is_playing);
        });
      });
    }
  };

  const handlePlayPause = () => {
    console.log("Play");
    spotifyApi.getMyCurrentPlaybackState().then((data) => {
      if (data.body.is_playing) {
        spotifyApi.pause();
        setIsPlaying(false);
      } else {
        spotifyApi.play();
        setIsPlaying(true);
      }
    });
  };

  useEffect(() => {
    if (spotifyApi.getAccessToken() && !currentTrackId) {
      fetchCurrentSong();
      setVolume(50);
    }
  }, [currentTrackIdState, spotifyApi, session]);

  useEffect(() => {
    if (volume > 0 && volume < 100) {
      debouncedAdjustVolume(volume);
    }
  }, [volume]);

  const debouncedAdjustVolume = useCallback(
    debounce((volume) => {
      spotifyApi.setVolume(volume).catch((err) => {});
    }, 500),
    []
  );

  useEffect(() => {
    debouncedAdjustTrackPosition(trackPosition);
  }, [trackPosition]);

  const debouncedAdjustTrackPosition = useCallback(
    debounce((trackPosition) => {
      console.log(trackPosition);
      spotifyApi.seek(trackPosition).catch((err) => {});
    }, 500),
    []
  );

  useEffect(() => {
    console.log("Shuffle");
    spotifyApi.setShuffle(shuffle);
  }, [shuffle]);

  const allowedRepeatStatus = [
    { status: "context", style: "text-green-500" },
    { status: "track", style: "text-green-200" },
    { status: "off", style: "text-white" },
  ];
  type repeatStatus = "track" | "context" | "off";

  const setRepeatStatus = () => {
    const currentIndex = allowedRepeatStatus.findIndex(
      (status) => status.status === repeat
    );
    const nextIndex = (currentIndex + 1) % allowedRepeatStatus.length;
    const nextStatus = allowedRepeatStatus[nextIndex].status as repeatStatus;

    console.log(nextStatus);
    console.log(allowedRepeatStatus[nextIndex].style);

    spotifyApi.setRepeat(nextStatus).then(function () {
      setRepeat(nextStatus);
      setRepeatColor(allowedRepeatStatus[nextIndex].style);
    });
  };

  const nextTrack = () => {
    console.log("Skip to next");
    spotifyApi.skipToNext().then((data) => {
      console.log(data);
    });
  };

  const previousTrack = () => {
    console.log("Previous Track");
    spotifyApi.skipToPrevious().then((data) => {
      console.log(data);
    });
  };

  return (
    <div className="grid h-24 grid-cols-3 px-2 text-xs text-white bg-gradient-to-b from-black to-gray-700 md:text-base md:px-8">
      <div className="flex items-center space-x-4">
        <img
          className="hidden w-10 h-10 md:inline"
          src={songInfo?.album.images?.[0]?.url}
          alt=""
        />
        <div>
          <h3>{songInfo?.name}</h3>
          <h3 className="text-xs">{songInfo?.artists?.[0]?.name}</h3>
        </div>
      </div>

      <div className="">
        <div className="flex items-center pb-4 justify-evenly">
          <SwitchHorizontalIcon
            onClick={() => setShuffle((prevCheck) => !prevCheck)}
            className={`button ${shuffle && "text-green-500"}`}
          />
          <RewindIcon onClick={previousTrack} className="button" />

          {isPlaying ? (
            <PauseIcon onClick={handlePlayPause} className="w-10 h-10 button" />
          ) : (
            <PlayIcon onClick={handlePlayPause} className="w-10 h-10 button" />
          )}

          <FastForwardIcon onClick={nextTrack} className="button" />
          <div>
            <ReplyIcon
              onClick={setRepeatStatus}
              className={`button ${repeatColor}`}
            />
          </div>
        </div>

        <div className="flex justify-between space-x-3">
          <div className="flex-grow text-right basis-0">0</div>
          <input
            className="w-96"
            type="range"
            value={trackPosition}
            min={0}
            max={songLength}
            step={1000}
            onChange={(e) => setTrackPosition(Number(e.target.value))}
          />
          <div className="flex-grow basis-0">{songLength}</div>
        </div>
      </div>

      <div className="flex items-center justify-end space-x-3 md:space-x-4">
        <VolumeDownIcon
          onClick={() => volume > 0 && setVolume(volume - 10)}
          className="button"
        />
        <input
          className="w-14 md:w-28"
          type="range"
          value={volume}
          min={0}
          max={100}
          onChange={(e) => setVolume(Number(e.target.value))}
        />
        <VolumeUpIcon
          onClick={() => volume < 100 && setVolume(volume + 10)}
          className="button"
        />
      </div>
    </div>
  );
}

export default Player;
