import { ChevronDownIcon } from "@heroicons/react/outline";
import { signOut, useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import { shuffle } from "lodash";
import { useRecoilState, useRecoilValue } from "recoil";
import { playlistIdState, playlistState } from "../atoms/playlistAtom";
import spotifyApi from "../lib/spotify";
import Songs from "./Songs";
import { Menu } from "@headlessui/react";

import FastAverageColor from "fast-average-color";

const colors = [
  "from-indigo-500",
  "from-blue-500",
  "from-green-500",
  "from-red-500",
  "from-yellow-500",
  "from-pink-500",
  "from-purple-500",
];

function Center() {
  const { data: session } = useSession();
  const [color, setColor] = useState(null);
  const playlistId = useRecoilValue(playlistIdState);
  const [playlist, setPlaylist] = useRecoilState(playlistState);
  //const fac = new FastAverageColor();

  useEffect(() => {
    // fac
    //   .getColorAsync(playlist?.images?.[0].url)
    //   .then((color) => {
    setColor(shuffle(colors).pop());
    // })
    // .catch((e) => {
    //   console.log(e);
    // });

    //setColor(shuffle(colors).pop());
  }, [playlist]);

  useEffect(() => {
    spotifyApi
      .getPlaylist(playlistId)
      .then((data) => {
        console.log(data);
        setPlaylist(data.body);
      })
      .catch((error) => console.log("Something went wrong!", error));
  }, [spotifyApi, playlistId]);

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide">
      <header className="absolute top-5 right-8">
        <div
          className="flex items-center bg-black space-x-3 opacity-90 hover:opacity-80 cursor-pointer text-white rounded-full p-1 pr-2"
          onClick={(e) => {
            signOut();
          }}
        >
          <img
            className="rounded-full w-10 h-10"
            src={session?.user.image}
            alt=""
          ></img>

          <h2>{session?.user.name}</h2>
          <ChevronDownIcon className="h-5 w-5" />
        </div>
      </header>

      <section
        className={`flex items-end space-x-7 bg-gradient-to-b to-black ${color} h-80 text-white p-8`}
      >
        <img
          className="h-44 w-44 shadow-2xl"
          src={playlist?.images?.[0].url}
          alt=""
        />
        <div>
          <p>PLAYLIST</p>
          <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold">
            {playlist?.name}
          </h1>
        </div>
      </section>

      <div className="flex justify-end pr-4">
        <Menu>
          <Menu.Button
            className={
              "bg-slate-500/30 p-2 rounded-lg text-gray-400 hover:text-white inline-flex justify-center"
            }
          >
            Sort
            <ChevronDownIcon
              className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
              aria-hidden="true"
            />
          </Menu.Button>
          <Menu.Items>
            <Menu.Item>
              {({ active }) => (
                <a className={`${active && "bg-blue-500"}`} href="#">
                  Recently Added
                </a>
              )}
            </Menu.Item>
          </Menu.Items>
        </Menu>
      </div>

      <div>
        <Songs />
      </div>
    </div>
  );
}

export default Center;
